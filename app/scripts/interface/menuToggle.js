if (!components) {
    var components = {};
}

components.menuToggle = Class.extend({

    options: {
        name : "_menuToggle"
    },

    init: function(el, options){
        this.el = $(el);
        this.options = $.extend(this.options, options);

        this.initialHeight = this.el.height();

        this.initElements();
        this.setEvents();
    },

    initElements: function() {
        this.$toggleTrigger = this.el.find('[data-toggle]');
    },

    setEvents: function() {
        if (this.$toggleTrigger) {
            this.$toggleTrigger.on('click', $.proxy(this.toggle, this));
        }
    },

    toggle: function(e) {
        e.preventDefault();
        var $target = $(e.currentTarget);

        if ($target.hasClass('open')) {
            $target.removeClass('open');
                 //  .height(0);
        } else {
            $target.addClass('open');
                 //  .height(this.initialHeight);
        }
    }
});

// jQuery plugin wrapper
$.fn.menuToggle = function ( options ) {
    return this.each(function() {
        if ( !$.data( this, "menuToggle") ) {
            $.data( this, "menuToggle", new components.menuToggle( this, options ) );
        }
    });
};
