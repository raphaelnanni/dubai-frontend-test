/* Navigation
 *
 * Manages toggle behaviors.
 *
 * Options :
 *
 * Dependencies :
 *  - jQuery
 *  - Modernizr
 *  - responsive hook
 *
 * */

if (!components) {
    var components = {};
}

components.navigation = Class.extend({

    options: {
        triggerNavClass : 'off-canvas-nav-trigger',
        levelSpacing : 40,
        type : 'overlap',
        scrollThrottle : 1, // in milliseconds
        name : "_navigation"
    },

    init: function(el, options){
        this.el = $(el);
        this.options = $.extend(this.options, options);

        // if menu is open or not
        this.open = false;
        // level depth
        this.level = 0;
        // event type (if mobile use touch events)
        this.eventtype = (Modernizr.touch && window.viewportSize != 'desktop') ? 'touchstart' : 'click';

        // init
        this.manipulateDOM();
        this.setElements();
        this.setEvents();
    },

    manipulateDOM: function() {
        // add mobile menu trigger markup
        $('#header > .inner').prepend('<a href="#" class="' + this.options.triggerNavClass + '">menu</a>');
    },

    setElements: function() {
        // the trigger
        this.$triggerNav = $('.' + this.options.triggerNavClass);
        // the moving wrapper
        this.$pusher = $('#container');
        // the menu items
        this.$menuItems = this.el.find( 'li' );
        // get header
        this.$header = $('#header');
        this.$logo = this.$header.find('#logo');
        // get nav
        this.$nav = $('#navigation');
    },

    setEvents: function() {
        // open the nav
        this.$triggerNav.on(this.eventtype, $.proxy(function(e){
            e.stopPropagation();
            e.preventDefault();

            if( this.open ) {
                this.resetMobileMenus();
            }
            else {
                this.openMobileNav();
            }
        }, this));

        this.$pusher.on(this.eventtype, $.proxy(function(e){
            if ($(e.currentTarget).hasClass('pushed')) {
                this.resetMobileMenus();
            }
        },this));
    },

    unsetEvents: function() {
        this.$menuItems.find('a').off(this.eventtype);
        this.$triggerNav.off(this.eventtype);
        this.$pusher.off(this.eventtype);
    },

    openMobileNav: function(subLevel) {
        // increment level depth
        ++this.level;

        // move the main wrapper
        var levelFactor = ( this.level - 1 ) * this.options.levelSpacing,
            translateVal = this.options.type === 'overlap' ? this.el.width() + levelFactor : this.el.width();

        this.setTransform( 'translate3d(' + translateVal + 'px,0,0)' );

        if (subLevel) {
            // reset transform for sublevel
            this.setTransform( '', subLevel );
            // need to reset the translate value for the level menus that have the same level depth and are not open
            for( var i = 0, len = this.$levels.length; i < len; ++i ) {
                var levelEl = this.$levels[i];
                if( levelEl != subLevel.get(0) && !$(levelEl).hasClass('level-open') ) {
                    this.setTransform( 'translate3d(-100%,0,0) translate3d(' + -1*levelFactor + 'px,0,0)', $(levelEl) );
                }
            }
        }

        // add class pushed to main wrapper if opening the first time
        if( this.level === 1 ) {
            this.$pusher.addClass( 'pushed' );
            this.open = true;
        }
    },

    resetMobileMenus: function() {
        this.setTransform('translate3d(0,0,0)');
        this.level = 0;
        // remove class mp-pushed from main wrapper
        this.$pusher.removeClass( 'pushed' );
        this.open = false;
    },

    closeMobileMenu: function() {
        var translateVal = this.options.type === 'overlap' ? this.el.width() + ( this.level - 1 ) * this.options.levelSpacing : this.el.width();
        this.setTransform( 'translate3d(' + translateVal + 'px,0,0)' );
        this.toggleLevels();
    },

    setTransform: function(val, el ) {
        el = (el != undefined) ? el.get(0) : this.$pusher.get(0);
        el.style.WebkitTransform = val;
        el.style.MozTransform = val;
        el.style.transform = val;
    }
});

// jQuery plugin wrapper
$.fn.navigation = function ( options ) {
    return this.each(function() {
        if ( !$.data( this, "navigation") ) {
            $.data( this, "navigation", new components.navigation( this, options ) );
        }
    });
};
