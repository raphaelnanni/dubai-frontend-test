
$(document).ready(function () {

    $('#navigation').navigation();

    $('.carousel').flexslider({
        animation: "slide",
        controlNav: "thumbnails",
        animationLoop: false,
        slideshow: false,
        start: function(slider) {
            // quick and dirty way to scale and center thumbnails after carousel instanciation
            for(var i=0; i<slider.controlNav.length; i++){
                imagesloaded(slider.controlNav[i], function(){
                    if (slider.controlNav[i].clientHeight > 0 && slider.controlNav[i].clientHeight < $(slider.controlNav[i]).parent()[0].clientHeight) {
                        $(slider.controlNav[i]).css({
                            height: $(slider.controlNav[i]).parent()[0].clientHeight,
                            width:'auto',
                            transition: 'none'
                        })
                    }
                    if(slider.controlNav[i].clientWidth > $(slider.controlNav[i]).parent()[0].clientWidth){
                        $(slider.controlNav[i]).css({
                            marginLeft: '-' + 1 * (slider.controlNav[i].clientWidth - $(slider.controlNav[i]).parent()[0].clientWidth) / 2 + 'px'
                        });
                    }
                });
            }
        }
    });

    $('#aside .menu').menuToggle();

});