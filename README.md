# README #

### Set up project ###

This project makes use of Yeoman, Bower and Grunt.

* Install [node.js](http://nodejs.org/)
* Install [yeoman](http://yeoman.io/learning/index.html)
* Clone the project from the git repository
* Go to the local project folder and install the project libraries:
    * server-side librairies ``` npm install ```
    * client-side librairies  ``` bower install ```

### Serve project ##

To serve and browse the project type the following command in a terminal :

```
#!shell

grunt serve
```

### Build project ###

To build a ready-to-deploy artifact type the following command in a terminal :

```
#!shell

grunt build
```

### Project features ###

#### Dev stack ####
* Full JS development stack
* Performance optimized assets build including concatenation, minification and fingerprinting
* Font-based vector icons (vectorized with Adobe Illustrator and generated with [Fontello](http://fontello.com/))

#### HTML ####
* HTML5 semantic and SEO friendly markup
* Lightweight markup optimized for mobile bandwidth

#### CSS ####
* LESS powered CSS3
* Color themes triggered with CSS class on <body> tag
* Full CSS3 animations and transitions
* Full responsive layout based on media queries built with "desktop-first" methodoloy
* Off-canvas navigation on mobile with fast-click trigger

#### Javascript ####
* Class-oriented Javascript code style
* Reusable components
* Touch-enabled carousel (with external lib [Flexslider](http://flexslider.woothemes.com/))
* Automatic carousel thumbnails sizing and cropping (only horizontal for the demo, could be extended to vertical)

### Potential improvements ###
* Automatic image weight optimization with imageOptim and Grunt
* Responsive image serving with Picture polyfill or the new srcset attribute
* Micro-data and microformat implementation to enhance search engines crawling
* Extended browser compatibility with graceful degradation on IE7-9
* Enhanced accessibility (No-js fallbacks, screen readers support, keyboard navigation...)

### Browser scope ###
Tested on the latest versions of Chrome, Firefox, IE, Safari, Opera, Chrome for Android, Safari iOS.